jQuery(function( $ ) {
  addRow = $('#promo_addRow');
  row = '<tr class="promo_row"><td><input type="text" placeholder="Naam..." /></td><td><input type="text" placeholder="Inhoud..." /></td><td><input type="text" placeholder="" /></td><td><input type="file" placeholder="" /></td><td colspan=3><input type=button value="x" class="button promo_removeRow" /> </td></tr>'

  $(addRow).click(function(){
    $('.promo_add').before(row);
    removeRow = $('.promo_removeRow');
    $(removeRow).click(function(){
      $(this).closest('.promo_row').remove();
    });
  });

  //metaboxes
  // $('#taxonomy-category.categorydiv').before($('#penny-promo-item-metabox #category-all'));
  // $('#taxonomy-category.categorydiv').before($('#penny-promo-item-metabox #category-adder'));
  // $('#taxonomy-category.categorydiv').remove();
});
