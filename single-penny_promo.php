<?php get_header(); ?>
<div class="<?php x_main_content_class(); ?>" role="main">

  <?php while ( have_posts() ) : the_post();

  function getMeta( $name ) {
    return get_post_meta( get_the_ID(), $name, true );
  }

  $promo_grid_alias = 'promos';

  $promo_postID = getMeta('eg-postid');
  $promo_heading_text = getMeta('eg-headingtext');
  $promo_button_text = getMeta('eg-buttontext');
  $promo_post_category = getMeta('eg-itemcategories');
  $promo_post_ids = getMeta('eg-itemids');
  $promo_post_category_display = implode( ', ', $promo_post_category );
  $promo_post_ids_display = implode( ', ', $promo_post_ids );
  $pink = 'hsl(339, 64%, 59%)';

  // Define section function and var
  function section($id = '', $class = '', $color = '') {
    return '[cs_section id="' . $id . '" class="' . $class . '" bg_color="' . $color . '" parallax="false" style="margin: 0px;padding: 45px 0px;"]';
  }
  $section_end = '[/cs_section]';

  // Define row function and var
  function row($id = '') {
    return '[cs_row id="' . $id . '" inner_container="true" marginless_columns="false" style="margin: 0px auto;padding: 0px;"]';
  }
  $row_end = '[/cs_row]';

  // Define column function and var
  function column($class = '', $size = '1/1') {
    return '[cs_column fade="false" fade_animation="in" fade_animation_offset="45px" fade_duration="750" type="' . $size . '" class="' . $class . '" style="padding: 0px;"]';
  }
  $column_end = '[/cs_column]';

  // Load grid with items defined in categories and ids text boxes
  function essGrid() {
    global $promo_post_category, $promo_post_ids, $promo_grid_alias;

    $promos = array();
    $output = array();

    $type_query = new WP_Query( array( 'post_type' => 'penny_promo_item') );

    while( $type_query->have_posts() ) : $type_query->the_post();
      $promos[] = get_the_id();
    endwhile;

    if ( $promo_post_category || $promo_post_ids ) {

      $cat_args = array( 'category__in' => $promo_post_category, 'post_type' => 'penny_promo_item' );
      $cat_query = new WP_Query( $cat_args );

      while($cat_query->have_posts()) : $cat_query->the_post();
        $output[] = get_the_id();
      endwhile;

      $ids_args = array( 'post__in' => $promo_post_ids, 'post_type' => 'penny_promo_item' );
      $ids_query = new WP_Query( $ids_args );

      while($ids_query->have_posts()) : $ids_query->the_post();
        $output[] = get_the_id();
      endwhile;

      if ( count($output) % 3 != 0) {
        $promo_grid_alias = 'promos-2';
      }

      if ( !$output) {
        $output = $promos;
      }
    } else {
      $output = $promos;
    }

    return do_shortcode('[ess_grid alias="' . $promo_grid_alias . '" posts="' . implode(',', $output) . '"]');
  }

  // Gather all the shortcodes
  $shortcodeContent = '';

  $shortcodeContent .= '[cs_content]';
  $shortcodeContent .= section('premie-large');
  $shortcodeContent .= row();
  $shortcodeContent .= column('1/1');
  $shortcodeContent .= '[x_custom_headline level="h2" looks_like="h2" accent="false" class="cs-ta-center fill-line"]' . $promo_heading_text . '[/x_custom_headline]';
  $shortcodeContent .= essGrid();
  $shortcodeContent .= $column_end;
  $shortcodeContent .= $row_end;
  $shortcodeContent .= row();
  $shortcodeContent .= column('cs-ta-center');
  $shortcodeContent .= '[x_button size="large" block="false" circle="false" icon_only="false" href="#over" title="" target="" info="none" info_place="right" info_trigger="hover" info_content="" class="mtl"]' . $promo_button_text . '[/x_button]';
  $shortcodeContent .= $column_end;
  $shortcodeContent .= $row_end;
  $shortcodeContent .= $section_end;
  $shortcodeContent .= section('over', 'svg-background', $pink);
  $shortcodeContent .= row();
  $shortcodeContent .= column('watispenny_outer');
  $shortcodeContent .= '[display-posts id="' . $promo_postID .'" image_size="large" include_excerpt="true" wrapper="div"]';
  $shortcodeContent .= $column_end;
  $shortcodeContent .= $row_end;
  $shortcodeContent .= $section_end;
  $shortcodeContent .= section('premie-small');
  $shortcodeContent .= row();
  $shortcodeContent .= column();
  $shortcodeContent .= '[x_custom_headline level="h2" looks_like="h2" accent="false" class="cs-ta-center fill-line"]Laatste stap[/x_custom_headline]';
  $shortcodeContent .= $column_end;
  $shortcodeContent .= $row_end;
  $shortcodeContent .= row();
  $shortcodeContent .= column('', '1/2');
  $shortcodeContent .= essGrid();
  $shortcodeContent .= $column_end;
  $shortcodeContent .= column('', '1/2');
  $shortcodeContent .= '[ess_grid_ajax_target alias="' . $promo_grid_alias . '"]';
  $shortcodeContent .= $column_end;
  $shortcodeContent .= $row_end;
  $shortcodeContent .= $section_end;
  $shortcodeContent .= '[/cs_content]';

  // Output entire page
  echo do_shortcode( $shortcodeContent );

endwhile; ?>

</div>

<?php get_footer(); ?>
