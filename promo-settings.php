<?php
add_action('admin_menu', 'promo_add_menu_entry');
function promo_add_menu_entry() {
  add_menu_page(
    'Penny Promo',
    'Penny Promo',
    'edit_posts',
    'promo-settings',
    'promo_settings_content',
    'dashicons-star-filled'
  );
}

function promo_settings_content() {
  ?>
  <div class="wrap">
    <h2><?php _e('Penny Promo&#39;s', 'penny-promo') ?></h2>

    <p>Vanuit dit scherm kan je Penny promo pagina's en de promo's die daarop worden weergegeven aanmaken.</p>
    <a ty class="button button-primary" href="edit.php?post_type=penny_promo">Promo's</a>
    <a class="button button-primary" href="edit.php?post_type=penny_promo_item" >Promo items</a>
  </div>
  <?php
}
