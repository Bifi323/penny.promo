<?php // Page content for main settings
add_action('admin_init', 'promo_init_settings_main');
function promo_init_settings_main() {
  add_settings_section(
    'promo_settings_main', // ID used to identify this section and with which to register options
    'Main Settings', // Title to be displayed on the administration page
    'promo_main_settings_callback', // Callback used to render the description of the section
    'promo-settings-main' // Page on which to add this section of options
  );
  add_settings_field( 'promo_post_name', 'Naam', 'promo_post_name_callback', 'promo-settings-main', 'promo_settings_main' );
  add_settings_field( 'promo_post_slug', 'URL', 'promo_post_slug_callback', 'promo-settings-main', 'promo_settings_main' );
  add_settings_field( 'promo_post_cat', 'Categorie', 'promo_post_cat_callback', 'promo-settings-main', 'promo_settings_main' );
  add_settings_field( 'promo_post_promos', 'Promos', 'promo_post_promos_callback', 'promo-settings-main', 'promo_settings_main' );
  register_setting( 'promo-settings-main', 'promo_post_name' );
  register_setting( 'promo-settings-main', 'promo_post_slug' );
  register_setting( 'promo-settings-main', 'promo_post_cat' );
  register_setting( 'promo-settings-main', 'promo_post_promos' );
}

function promo_main_settings_callback() {
  echo 'Main description area.';
}

function promo_post_name_callback() {
  $options = get_option( 'promo_post_name' );
  echo '<input type="text" placeholder="Naam..." />';
}

function promo_post_slug_callback() {
  $options = get_option( 'promo_post_slug' );
  echo 'penny.promo/<input type="text" placeholder="nieuweURL" />';
}

function promo_post_cat_callback() {
  $options = get_option( 'promo_post_cat' );
  echo 'Categorie: <input type="text" placeholder="winter" /><br />+ losse IDs: <input type="text" placeholder="11, 23, 24" />';
  echo '<p class="description" id="home-description">Vul hier de categorie en optionele losse IDs van de promos in. De ID van een post kan je zien in de URL. Voorbeeld: http://penny.promo/wp-admin/post.php<strong>?post=44</strong>&action=edit</p>';
}

function promo_post_promos_callback() {
  $options = get_option( 'promo_post_promos' );
  echo '
  <table class="sub-table">
    <tr>
      <th>
        Naam
      </th>
      <th>
        iFrame URL
      </th>
      <th>
      Afbeelding
      </th>
      <th></th>
    </tr>
    <tr class="promo_add">
    <td colspan=4>
    <input type=button value="+" class="button" id="promo_addRow" />
    </td>
    </tr>
  </table>
  ';
}

function promo_settings_main_content() {

}

function promo_add_promo() {
  $hierarchical_tax = array( 13, 10 );
  $non_hierarchical_tax = 'tax name 1, tax name 2';

  $post_arr = array(
    'post_title'   => 'Test post',
    'post_type'    => 'penny_promo',
    'post_content' => 'Test post content',
    'post_status'  => 'publish',
    'post_author'  => get_current_user_id(),
    'tax_input'    => array(
      'hierarchical_tax'     => $hierarchical_tax,
      'non_hierarchical_tax' => $non_hierarchical_tax,
    ),
    'meta_input'   => array(
      'test_meta_key' => 'value of test_meta_key',
    ),
  );
  wp_insert_post( $post_arr );
}
