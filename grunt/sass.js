module.exports = {
	prod: {
		options: {
			loadPath: 'assets/scss/partials',
			style: 'expanded', 
		},
		files: {
			'dist/css/promo.css': 'assets/scss/style.scss',
		},
	},
	dist: {
		options: {
			sourcemap: 'none',
			loadPath: 'assets/scss/partials',
		},
		files: {
			'dist/css/promo.css': 'assets/scss/style.scss'
		}
	}
};
