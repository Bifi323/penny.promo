module.exports = {
  options:  {
    shorthandCompacting:  false,
    roundingPrecision:  -1,
  },
  target:  {
    files:  {
      'style.min.css': 'style.css'
    }
  }
};
