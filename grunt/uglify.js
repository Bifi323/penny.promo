module.exports = {
  options: {
    sourceMap: true,
  },
  dist: {
    files: {
      'dist/js/script.min.js': 'assets/js/concatenated.js'
    }
  }
};
