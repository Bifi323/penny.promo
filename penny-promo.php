<?php
/**
* Plugin Name: Penny Promo
* Description: PHP
* Version: 1.0.0
* Author: Edie Lemoine
* Author URI: http://onetwo.link/
* License: GPL2
*/

include 'promo-settings.php'; // Main settings
include 'promo-meta.php'; // Other meta settings
include 'promo-metabox.php'; // Adds promo metabox and handling
include 'promo-item-metabox.php'; // Adds promo item metabox and handling
// include 'promo-settings-main.php'; // Main settings page
include 'promo-display-posts-shortcode.php'; // Shortcode for rendering a post
include 'promo-svg.php'; // Enables SVG support in media library

// Enqueue admin js and css
function promo_admin_files() {
  wp_enqueue_style( 'promo-admin-theme', plugins_url('dist/css/promo.css', __FILE__), array(), '1.1.6' );
  wp_enqueue_script( 'promo-admin-script', plugins_url( "dist/js/promo.min.js",   __FILE__ ), array( 'jquery' ), '1.1.6' );
}
add_action( 'admin_enqueue_scripts', 'promo_admin_files' );

//Custom Post Types
add_action( 'login_enqueue_scripts', 'promo_admin_files' );
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'penny_promo',
    array(
      'labels' => array(
        'name' => __( 'Promo pagina&#39;s' ),
        'singular_name' => __( 'Promo pagina' )
      ),
      'supports' => array( 'title' ),
      'public' => true,
      'has_archive' => false,
    )
  );
  register_post_type( 'penny_promo_item',
    array(
      'labels' => array(
        'name' => __( 'Promo&#39;s' ),
        'singular_name' => __( 'Promo' )
      ),
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'taxonomies'  => array( 'category' ),
      'public' => true,
      'has_archive' => false,
    )
  );
}

// Removes /penny_promo/ slug from promo pages
function promo_remove_slug( $post_link, $post, $leavename ) {
  if ( 'penny_promo' != $post->post_type || 'publish' != $post->post_status ) {
    return $post_link;
  }

  $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
  return $post_link;
}
add_filter( 'post_type_link', 'promo_remove_slug', 10, 3 );

function promo_parse_request_trick( $query ) {
  if ( ! $query->is_main_query() )
    return;
  if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
    return;
  }
  if ( ! empty( $query->query['name'] ) ) {
    $query->set( 'post_type', array( 'post', 'page', 'penny_promo' ) );
  }
}
add_action( 'pre_get_posts', 'promo_parse_request_trick' );

// Tells promo pages to use single-penny_promo.php
add_filter('single_template', 'promo_page_template', 99);
function promo_page_template($template) {
  global $post;

  if ($post->post_type == "penny_promo"){

    $plugin_path = plugin_dir_path( __FILE__ );
    $template_name = 'single-penny_promo.php';

    if($template === get_stylesheet_directory() . '/' . $template_name
      || !file_exists($plugin_path . $template_name)) {

      return $template;
    }
    return $plugin_path . $template_name;
  }
  return $template;
}


add_shortcode('promo_post_type', 'promo_post_type_shortcode');

function promo_post_type_shortcode( $post ) {
  echo get_post_type();
}

// function promo_overwrite_shortcode()
// {
//     function promo_register_shortcode_ajax_target( $args, $mid_content=null ) {
//       $args = apply_filters('essgrid_register_shortcode_ajax_target_pre', $args);
//
//   		extract(shortcode_atts(array('alias' => ''), $args, 'ess_grid_ajax_target'));
//
//   		if($alias == '') return false; //no alias found
//
//   		$output_protection = get_option('tp_eg_output_protection', 'none');
//
//   		$content = '';
//
//   		$grid = new Essential_Grid;
//
//   		$grid_id = self::get_id_by_alias($alias);
//   		if($grid_id > 0){
//
//   			$grid->init_by_id($grid_id);
//   			//check if shortcode is allowed
//
//   			$is_sc_allowed = $grid->get_param_by_handle('ajax-container-position');
//   			if($is_sc_allowed != 'shortcode') return false;
//
//   			$content = $grid->output_ajax_container();
//
//   		}
//   		//handle output types
//   		switch($output_protection){
//   			case 'compress':
//   				$content = str_replace("\n", '', $content);
//   				$content = str_replace("\r", '', $content);
//   				return($content);
//   			break;
//   			case 'echo':
//   				echo $content;		//bypass the filters
//   			break;
//   			default: //normal output
//   				return($content);
//   			break;
//   		}
//   }
//   remove_shortcode('ess_grid_ajax_target');
//   add_shortcode('ess_grid_ajax_target', 'promo_register_shortcode_ajax_target');
// }
// add_action( 'wp_loaded', 'promo_overwrite_shortcode' );
