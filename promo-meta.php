<?php
//Remove Essential Grid and Revolution slider meta boxes from all post types
function promo_remove_esg_rev_metabox() {
  if( is_admin() && current_user_can('manage_options') ) {
  $args = array(
   'public'   => true,
  );
  $output = 'names';
  $operator = 'and';
  $post_types = get_post_types( $args, $output, $operator );
    foreach ( $post_types  as $post_type ) {
      remove_meta_box('eg-meta-box', $post_type, 'normal');
      remove_meta_box('mymetabox_revslider_0', $post_type, 'normal');
    }
  }
}
add_action('add_meta_boxes', 'promo_remove_esg_rev_metabox', 999);

// Add iFrame to content
add_filter( 'the_content', 'promo_iframe_after_content' );
function promo_iframe_after_content( $content ) {
  if ( get_post_type( get_the_ID() ) == 'penny_promo_item') {

    $iframe = get_post_meta( get_the_ID(), 'eg-iframe', true );
    $content = $content . '<iframe id=iframe src="' . $iframe . '"></iframe>';

    return $content;
  }
}
//
// // Allowed PHP tags in excerpt
// add_filter( 'promo_allowed_excerpt_tags', 'promo_excerpt_tags', 100, 1 );
// function promo_excerpt_tags() {
// // Add custom tags to this string
//     return '<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<iframe>';
// }
