<?php

function promo_svg_types($svgs) {
  $svgs['svg'] = 'image/svg+xml';
  return $svgs;
}
add_filter('upload_svgs', 'promo_svg_types');

?>
