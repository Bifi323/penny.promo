<?php

function promo_validate( $input ) {

  // Create our array for storing the validated options
  $output = array();

  // Loop through each of the incoming options
  foreach( $input as $key => $value ) {

    // Check to see if the current option has a value. If so, process it.
    if( isset( $input[$key] ) ) {

      // Strip all HTML and PHP tags and properly handle quoted strings
      $output[$key] = strip_tags( stripslashes( $input[ $key ] ) );

    } // end if

  } // end foreach

  // Return the array processing any additional functions filtered by this action
  return apply_filters( 'promo_validate', $output, $input );

}

function promo_check_upload($options)
{
   //check if user had uploaded a file and clicked save changes button
   if(!empty($_FILES["background_picture"]["tmp_name"]))
   {
       $urls = wp_handle_upload($_FILES["background_picture"], array('test_form' => FALSE));
       $temp = $urls["url"];
       return $temp;
   }

   //no upload. old file url is the new value.
   return get_option("background_picture");
}

?>
