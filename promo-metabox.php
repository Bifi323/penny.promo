<?php

// Promo metabox
function promo_add_meta_box($post) {
  add_meta_box( 'promo_meta', 'Instellingen', 'promo_meta_callback', 'penny_promo' );
}
add_action( 'add_meta_boxes', 'promo_add_meta_box' );

// Rendering callback
function promo_meta_callback( $post ) {
  wp_nonce_field( basename( __FILE__ ), 'promo_meta_nonce' );

  $current_postid = get_post_meta( $post->ID, 'eg-postid', true );
  $current_buttontext = get_post_meta( $post->ID, 'eg-buttontext', true );
  $raw_cats = get_post_meta( $post->ID, 'eg-itemcategories', true );
  $raw_ids = get_post_meta( $post->ID, 'eg-itemids', true );
  $current_cats = implode( ', ', $raw_cats );
  $current_ids = implode( ', ', $raw_ids );

  ?>
	<div class='inside' id='penny-promo-metabox'>

    <h4><?php _e( 'Banner', 'penny-promo' ); ?></h4>
    <p>
      Vul hier het ID in van de post die je in de roze balk wilt weergeven. Standaardwaarde is 44 (Over Penny).
    </p>
    <input type="text" name="postid" placeholder='44' value="<?php echo $current_postid; ?>" />

    <h4><?php _e( 'Knop tekst', 'penny-promo' ); ?></h4>
    <p>
      De tekst op de knop die doorverwijst naar de roze balk. Standaardwaarde: "Meer over Penny".
    </p>
    <input type="text" name="buttontext" placeholder='Meer over Penny' value="<?php echo $current_buttontext; ?>" />

    <h4><?php _e( 'Promos', 'penny-promo' ); ?></h4>
    <p>Vul hier de categorieen en/of losse IDs in die je wilt includeren in de premie's voor deze pagina. Als je deze leeg laat verschijnen alle promo's uit alle categorieën.</p>
    <table>
      <tr>
        <td>
          <label>Categorieen: </label>
        </td>
        <td>
          <input type="text" name="cats" placeholder='1, 5' value="<?php echo $current_cats; ?>" />
        </td>
      </tr>
      <tr>
        <td>
          <label>IDs: </label>
        </td>
        <td>
          <input type="text" name="ids" placeholder='21, 24, 25' value="<?php echo $current_ids; ?>" />
        </td>
      </tr>
    </table>
    <p class="description">
      Het ID van een post vind je in de instellingen of in de URL. Het ID van een categorie vind je alleen in de URL. Voorbeeld: http://penny.promo/wp-admin/post.php?post=<strong>234</strong>&action=edit
    </p>
  </div>
<?php
}

// On save_post
function promo_save_meta_box( $post_id ) {
	if ( !isset( $_POST['promo_meta_nonce'] ) || !wp_verify_nonce( $_POST['promo_meta_nonce'], basename( __FILE__ ) ) ){
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}
  if ( isset( $_REQUEST['postid'] ) ) {
    if ( $_POST['postid'] == '' ) {
      update_post_meta( $post_id, 'eg-postid',  44 );
    } else {
		  update_post_meta( $post_id, 'eg-postid',  $_POST['postid'] );
    }
	}
  if ( isset( $_REQUEST['buttontext'] ) ) {
    if ( $_POST['postid'] == '' ) {
      update_post_meta( $post_id, 'eg-buttontext', 'Meer over Penny' );
    } else {
      update_post_meta( $post_id, 'eg-buttontext',  $_POST['buttontext'] );
    }
	}
  function promo_clean( $value ) {
    return explode( ', ', strip_tags( trim( $_POST[$value] )));
  }

  if ( isset( $_REQUEST['cats'] ) ) {
		update_post_meta( $post_id, 'eg-itemcategories', promo_clean( 'cats' ));
	}
  if ( isset( $_REQUEST['ids'] ) ) {
		update_post_meta( $post_id, 'eg-itemids', promo_clean( 'ids' ) );
	}
}
add_action( 'save_post', 'promo_save_meta_box', 10, 2 );
