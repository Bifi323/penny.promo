<?php
// Promo ITEM metabox
function promo_item_add_meta_box($post) {
  add_meta_box( 'promo_item_meta', 'Instellingen', 'promo_item_meta_callback', 'penny_promo_item' );
}
add_action( 'add_meta_boxes', 'promo_item_add_meta_box' );

// Rendering Callback
function promo_item_meta_callback( $post ) {
  wp_nonce_field( basename( __FILE__ ), 'promo_item_meta_nonce' );

	$current_iframe = get_post_meta( $post->ID, 'eg-iframe', true );
	?>

	<div class='inside' id='penny-promo-item-metabox'>
    <h4><?php _e( 'iFrame URL', 'penny-promo' ); ?></h4>
		<p class=description>
      Plak hier de hele URL naar de site met het formulier.<br />
			<input type="text" name="iframe" value="<?php echo $current_iframe; ?>" />
		</p>
    <p>Het ID van deze premie is: <?php echo $post->ID; ?></p>
  </div>
<?php
}

// On save_post
function promo_item_save_meta_box( $post_id ) {
	if ( !isset( $_POST['promo_item_meta_nonce'] ) || !wp_verify_nonce( $_POST['promo_item_meta_nonce'], basename( __FILE__ ) ) ){
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}
	if ( isset( $_REQUEST['iframe'] ) ) {
		update_post_meta( $post_id, 'eg-iframe', esc_url( $_POST['iframe'] ) );
	}
}
add_action( 'save_post', 'promo_item_save_meta_box', 10, 2 );
